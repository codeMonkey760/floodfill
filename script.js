var boardDiv = undefined;
var tileArray = undefined;

// width and height in tiles
var width = 25;
var height = 25;

// mouse tracking
var mouseDown = false;

function main() {
	initBoard();
}

function initBoard() {
	tileArray = [];
	boardDiv = document.createElement("DIV");
	boardDiv.style.width = (width * 25) + "px";
	addEvent(boardDiv, "mousedown", function(event) {handleMouse(event,true);});
	addEvent(boardDiv, "mouseup", function(event) {handleMouse(event,false);});
	
	for (var i = 0; i < width; ++i) {
		tileArray[i] = [];
		for (var j = 0; j < height; ++j) {
			var img = document.createElement("IMG");
			img.src = "Tile1.bmp";
			img.style.verticalAlign = "top";
			img.color = "red";
			img.flipTile = flipTile;
			img.floodFill = floodFill;
			img.tx = i;
			img.ty = j;
			boardDiv.appendChild(img);
			tileArray[i][j] = img;

			// add event handlers
			addEvent(img, "contextmenu", function(event) {
				event.preventDefault();
				return false;
			});
			addEvent(img, "mousedown", function(event){
				if (event.button == 0) {
					this.flipTile(false);
				}
			});
			addEvent(img, "mouseup", function(event) {
				if (event.button == 2) {
					this.floodFill();
				}
			});
			addEvent(img, "mouseover", function(){
				this.flipTile(true);
			});
			addEvent(img, "click", function(){
				this.flipTile(false);
			});
			addEvent(img, "dragstart", function(event){
				event.preventDefault();
			});
		}
		boardDiv.appendChild(document.createElement("BR"));
	}

	if (document.body != undefined) {
		document.body.appendChild(boardDiv);
	}
}

function floodFill() {
	var floodQ = [];
	var curState = this.color;
	var comp = function(i,j) {
		if (tileArray[i][j] != undefined) {
			if (tileArray[i][j].color == curState) {
				tileArray[i][j].flipTile(false);
				return true;
			}
		}
		return false;
	}
	var pushTile = function(i,j) {
		if (i > -1 && i < width && j > -1 && j < height) {
			floodQ.push(tileArray[i][j]);
		}
	}

	floodQ.push(this);
	while(floodQ.length >= 0) {
		curTile = floodQ.shift();
		x = curTile.tx;
		y = curTile.ty;
		if (comp(x,y)) {
			pushTile(x-1,y);
			pushTile(x+1,y);
			pushTile(x,y-1);
			pushTile(x,y+1);
		}		
		
	}
}

function addEvent(element, event, func) {
	if (element.attachEvent != undefined) {
		element.attachEvent("on" + event, func);
	} else {
		element.addEventListener(event, func, false);
	}
}

function flipTile(checkMouse) {
	if (mouseDown == true || checkMouse == false) {
		if (this.color == "red") {
			this.color = "green";
			this.src = "Tile2.bmp";
		} else {
			this.color = "red";
			this.src = "Tile1.bmp";
		}
	}
}

function handleMouse(event, down) {
	if (event.button == 0) {
		mouseDown = down;
	}
}
